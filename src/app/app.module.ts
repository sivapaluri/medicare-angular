import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { ProductComponent } from './Components/product/product.component';
import { HomeComponent } from './Components/home/home.component';
import { SearchmedicineComponent } from './Components/searchmedicine/searchmedicine.component';
import { CartComponent } from './Components/cart/cart.component';
import { AdminhomeComponent } from './Components/adminhome/adminhome.component';
import { AdmAddProductComponent } from './Components/adm-add-product/adm-add-product.component';
import { AdmDelProductComponent } from './Components/adm-del-product/adm-del-product.component';
import { AdmAllProductsComponent } from './Components/adm-all-products/adm-all-products.component';
import { AdmEditProductsComponent } from './Components/adm-edit-products/adm-edit-products.component';
import { AdmDisableProductsComponent } from './Components/adm-disable-products/adm-disable-products.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProductComponent,
    HomeComponent,
    SearchmedicineComponent,
    CartComponent,
    AdminhomeComponent,
    AdmAddProductComponent,
    AdmDelProductComponent,
    AdmAllProductsComponent,
    AdmEditProductsComponent,
    AdmDisableProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

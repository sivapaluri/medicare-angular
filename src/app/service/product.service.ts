import { Injectable } from '@angular/core';
import { Product } from '../model/product';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url: string;
  product: Product;
  constructor(private http: HttpClient) {
  }
  public getAllProducts(): Observable<Product[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Product[]>("http://localhost:8080/adm-inv", { headers });
  }

  public getMedicineOnSearch(type: String): Observable<Product[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Product[]>("http://localhost:8080/cs-invm/" + type, { headers });
  }

}



import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adm-edit-products',
  templateUrl: './adm-edit-products.component.html',
  styleUrls: ['./adm-edit-products.component.css']
})
export class AdmEditProductsComponent implements OnInit {
  product:Product;

  constructor(private http:HttpClient, private router:Router) {
    this.product = new Product;
   }

   public editProduct(){
    let username='admin'
      let password='admin123'
      const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
 this.http.patch<Product>('http://localhost:8080/adm-inv',this.product,{headers}).subscribe(data=>{
        console.log(this.product);
        this.router.navigate(['/allproducts'])
  })
}


  ngOnInit() {
  }

}

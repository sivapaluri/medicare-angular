import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products:Product[];
  constructor(private service:ProductService) { }

  ngOnInit() {
    return this.service.getAllProducts().subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}
}

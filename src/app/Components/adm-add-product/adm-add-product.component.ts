import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductService } from 'src/app/service/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adm-add-product',
  templateUrl: './adm-add-product.component.html',
  styleUrls: ['./adm-add-product.component.css']
})
export class AdmAddProductComponent implements OnInit {
product:Product;
  constructor(private http:HttpClient,private router:Router) {
    this.product = new Product;
   }

  public createProduct(){
    let username='admin'
      let password='admin123'
      const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
 this.http.post<Product>('http://localhost:8080/adm-inv',this.product,{headers}).subscribe(data=>{
        console.log(this.product);
        this.router.navigate(['/adminhome'])
  })
}

  ngOnInit() {
  }

}

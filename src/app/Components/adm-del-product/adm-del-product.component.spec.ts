import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDelProductComponent } from './adm-del-product.component';

describe('AdmDelProductComponent', () => {
  let component: AdmDelProductComponent;
  let fixture: ComponentFixture<AdmDelProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmDelProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmDelProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

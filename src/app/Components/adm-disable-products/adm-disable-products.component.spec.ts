import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDisableProductsComponent } from './adm-disable-products.component';

describe('AdmDisableProductsComponent', () => {
  let component: AdmDisableProductsComponent;
  let fixture: ComponentFixture<AdmDisableProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmDisableProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmDisableProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

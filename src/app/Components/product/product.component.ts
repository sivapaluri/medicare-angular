import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
   products:Product[];
 
  medicineName:String;
  constructor(private service:ProductService) { }

  getMedicineOnSearch(){
    return this.service.getMedicineOnSearch(this.medicineName).subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}

getAllProducts(){
  return this.service.getAllProducts().subscribe(data=>{
    this.products=data;
    console.log(data);
})
}

  ngOnInit() {
    return this.service.getAllProducts().subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}
}

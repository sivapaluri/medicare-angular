import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-adm-all-products',
  templateUrl: './adm-all-products.component.html',
  styleUrls: ['./adm-all-products.component.css']
})
export class AdmAllProductsComponent implements OnInit {
  products:Product[];
 
  medicineName:String;
  constructor(private service:ProductService) { }

  getMedicineOnSearch(){
    return this.service.getMedicineOnSearch(this.medicineName).subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}

getAllProducts(){
  return this.service.getAllProducts().subscribe(data=>{
    this.products=data;
    console.log(data);
})
}

  ngOnInit() {
    return this.service.getAllProducts().subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}
}

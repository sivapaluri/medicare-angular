import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmAllProductsComponent } from './adm-all-products.component';

describe('AdmAllProductsComponent', () => {
  let component: AdmAllProductsComponent;
  let fixture: ComponentFixture<AdmAllProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmAllProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAllProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

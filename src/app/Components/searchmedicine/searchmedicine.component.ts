import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product.service';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-searchmedicine',
  templateUrl: './searchmedicine.component.html',
  styleUrls: ['./searchmedicine.component.css']
})
export class SearchmedicineComponent implements OnInit {
   products:Product[];
   products1:Product[];
   medicineName:string;
  constructor(private service:ProductService) { 
    
  }

  getMedicineOnSearch(){
    return this.service.getMedicineOnSearch(this.medicineName).subscribe(data=>{
      this.products=data;
      console.log(data);
  })
}

getAllProducts(){
  return this.service.getAllProducts().subscribe(data=>{
    this.products1=data;
    console.log(data);
})
}

  ngOnInit() {
    
  }

}

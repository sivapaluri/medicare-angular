export class Product {

    inventoryId:number;
    medicineName:string;
    quantity:number;
    price:number;
    manufactureDate:string;
    expiryDate:string;
    batchNo:string;
    isEnabled:string;
    type:string;
    url:string;
}

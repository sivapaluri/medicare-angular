import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { HomeComponent } from './Components/home/home.component';
import { ProductComponent } from './Components/product/product.component';
import { SearchmedicineComponent } from './Components/searchmedicine/searchmedicine.component';
import { CartComponent } from './Components/cart/cart.component';
import { AdminhomeComponent } from './Components/adminhome/adminhome.component';
import { AdmAddProductComponent } from './Components/adm-add-product/adm-add-product.component';
import { AdmDelProductComponent } from './Components/adm-del-product/adm-del-product.component';
import { AdmEditProductsComponent } from './Components/adm-edit-products/adm-edit-products.component';
import { AdmAllProductsComponent } from './Components/adm-all-products/adm-all-products.component';
import { AdmDisableProductsComponent } from './Components/adm-disable-products/adm-disable-products.component';


const routes: Routes = [

  {path: "", redirectTo: "home", pathMatch: "full"},
  {path: "adminhome", redirectTo: "adminhome", pathMatch: "full"},
  {path: "home", component:HomeComponent },
  {path: "addproduct", component:AdmAddProductComponent },
  {path: "delproduct", component:AdmDelProductComponent },
  {path: "updateproduct", component:AdmEditProductsComponent },
  {path: "allproducts", component:AdmAllProductsComponent },
  {path: "disableproduct", component:AdmDisableProductsComponent },
  {path: "adminhome", component:AdminhomeComponent },
  {path: "login", component:LoginComponent },
  {path: "product", component:ProductComponent},
  {path: "searchmedicine", component:SearchmedicineComponent},
  {path: "cart", component:CartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
